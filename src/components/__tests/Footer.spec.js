import { shallowMount, createLocalVue } from '@vue/test-utils'
import Footer from '../Footer'
import Vuetify from 'vuetify'

describe('Footer', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    wrapper = shallowMount(Footer, { localVue })
  })

  describe('When footer is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
