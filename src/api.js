import axios from 'axios'
const hostname = 'http://localhost:3000'

const api = {
  getAllRecipes: async (onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/all`
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  },
  getRecipeById: async (id, onSuccess, onError) => {
    try {
      let response = await axios({
        method: 'GET',
        url: `${hostname}/recipe/${id}`
      })
      return onSuccess(response.data)
    } catch (error) {
      return onError(error)
    }
  }
}

export default api
