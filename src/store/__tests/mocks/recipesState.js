const state = {
  list: [
    {
      id: '1',
      name: 'Test recipe 1',
      creationDate: '1531958400',
      picture: 'testPictureSrc1',
      isVegetarian: false,
      numberOfPeople: 3,
      ingredients: 'Test ingredients 1',
      cookingInstructions: 'Test cooking instructions 1'
    },
    {
      id: '2',
      name: 'Test recipe 2',
      creationDate: '1531872000',
      picture: 'testPictureSrc2',
      isVegetarian: true,
      numberOfPeople: 2,
      ingredients: 'Test ingredients 2',
      cookingInstructions: 'Test cooking instructions 2'
    }
  ],
  minNumberOfPeople: 0,
  showOnlyVegetarian: false,
  searchString: ''
}

export default state
