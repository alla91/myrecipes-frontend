import { shallowMount, createLocalVue } from '@vue/test-utils'
import FilterBy from '../FilterBy'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('FilterBy', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({})
    wrapper = shallowMount(FilterBy, { store, localVue })
  })

  describe('When filterBy is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Computed', () => {
    describe('showOnlyVegetarian', () => {
      it('gets the default value', () => {
        expect(wrapper.vm.showOnlyVegetarian).toBe(false)
      })

      it('sets the correct value and dispatches filterVegetarian action', () => {
        wrapper.vm.$store.dispatch = jest.fn()
        wrapper.vm.showOnlyVegetarian = true
        expect(wrapper.vm.$store.dispatch).toHaveBeenCalledWith('recipes/filterVegetarian', { vegetarian: true })
      })
    })

    describe('minNumberOfPeople', () => {
      it('gets the default value', () => {
        expect(wrapper.vm.minNumberOfPeople).toBe(0)
      })

      it('sets the correct value and dispatches filterMinNumberOfPeople action', () => {
        wrapper.vm.$store.dispatch = jest.fn()
        wrapper.vm.minNumberOfPeople = 2
        expect(wrapper.vm.$store.dispatch).toHaveBeenCalledWith('recipes/filterMinNumberOfPeople', { min: 2 })
      })
    })
  })
})
