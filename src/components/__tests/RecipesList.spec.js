import { shallowMount, createLocalVue } from '@vue/test-utils'
import RecipesList from '../RecipesList'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('RecipesList', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({
      getters: {
        'recipes/getSearchString': () => 'test'
      }
    })
    wrapper = shallowMount(RecipesList, {
      store,
      localVue,
      propsData: {
        recipes: [
          {
            _id: 1
          },
          {
            _id: 2
          }
        ]
      }
    })
  })

  describe('When RecipesList is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
