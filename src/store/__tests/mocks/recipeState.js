const state = {
  id: '1',
  name: 'Test recipe',
  creationDate: '1531958400',
  picture: 'testPictureSrc',
  isVegetarian: false,
  numberOfPeople: 3,
  ingredients: 'Test ingredients',
  cookingInstructions: 'Test cooking instructions'
}

export default state
