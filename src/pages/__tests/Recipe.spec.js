import { shallowMount, createLocalVue } from '@vue/test-utils'
import Recipe from '../Recipe'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('Recipe', () => {
  let store
  let localVue
  let wrapper
  let $route

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({
      getters: {
        'recipe/getName': () => 'Test name',
        'recipe/getPicture': () => 'pictureSrc',
        'recipe/getIsVegetarian': () => false,
        'recipe/getNumberOfPeople': () => 2,
        'recipe/getIngredients': () => 'Test ingredients',
        'recipe/getCookingInstructions': () => 'Test cooking instructions',
        'recipe/getCreationDate': () => 1531958400
      },
      actions: {
        'recipe/getRecipeById': () => {}
      }
    })
    $route = { params: { id: '1' } }
    wrapper = shallowMount(Recipe, { store, localVue, mocks: { $route } })
  })

  describe('When Recipe page is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Computed', () => {
    describe('recipe name', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.name).toEqual('Test name')
      })
    })
    describe('recipe picture', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.picture).toEqual('pictureSrc')
      })
    })
    describe('recipe is vegeterian', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.isVegetarian).toEqual(false)
      })
    })
    describe('recipe number of people', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.numberOfPeople).toEqual(2)
      })
    })
    describe('recipe ingredients', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.ingredients).toEqual('Test ingredients')
      })
    })
    describe('recipe cooking instructions', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.cookingInstructions).toEqual('Test cooking instructions')
      })
    })
    describe('recipe creation date is correctly formatted', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.creationDate).toEqual('19 Jul, 2018')
      })
    })
  })
})
