import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import recipes from '../../modules/recipes'
import recipesStateMock from '../mocks/recipesState'
import * as types from '../../mutation-types'
const hostname = 'http://localhost:3000'

describe('Recipes', () => {
  let API = new MockAdapter(axios)

  describe('State', () => {
    it('should contain the same keys as the mock', () => {
      let keysInModule = Object.keys(recipes.state)
      let keysInMock = Object.keys(recipesStateMock)
      expect(keysInModule).toEqual(keysInMock)
    })
  })

  describe('Getters', () => {
    let state
    beforeEach(() => {
      state = {...recipesStateMock}
    })

    it('returns the recipes list on getRecipesList', () => {
      const getterResult = recipes.getters.getRecipesList(state)
      expect(getterResult).toEqual(recipesStateMock.list)
    })

    it('returns the searchString on getSearchString', () => {
      const getterResult = recipes.getters.getSearchString(state)
      expect(getterResult).toEqual(recipesStateMock.searchString)
    })
  })

  describe('Actions', () => {
    let commit = {
      count: 0,
      payload: ''
    }

    beforeEach(() => {
      commit.count = 0
      commit.payload = ''
    })

    afterEach(() => {
      API.reset()
    })

    describe('getAllRecipes', () => {
      let path = `${hostname}/all`
      let mockResponse = {
        recipes: [
          {
            _id: '1',
            name: 'Test recipe 1',
            creationDate: '1531958400',
            picture: 'testPictureSrc1',
            vegetarian: false,
            numberOfPeople: 1,
            ingredients: 'Test ingredients 1',
            cookingInstructions: 'Test cooking instructions 1'
          }
        ]
      }
      describe('onSuccess', () => {
        let mockCommit = (type) => {
          if (type === 'RECEIVE_RECIPES') {
            commit.count += 1
          }
        }

        it('it commits RECEIVE_RECIPES with no filters applied', async () => {
          API.onGet(path).reply(200, mockResponse)
          await recipes.actions.getAllRecipes({commit: mockCommit, state: recipesStateMock})
          expect(commit.count).toEqual(1)
        })

        it('it commits RECEIVE_RECIPES with vegetarian filter applied', async () => {
          API.onGet(path).reply(200, mockResponse)
          await recipes.actions.getAllRecipes({
            commit: mockCommit,
            state: {
              list: recipesStateMock.list,
              minNumberOfPeople: recipesStateMock.minNumberOfPeople,
              showOnlyVegetarian: true,
              searchString: recipesStateMock.searchString
            }
          })
          expect(commit.count).toEqual(1)
        })

        it('it commits RECEIVE_RECIPES with number of people filter applied', async () => {
          API.onGet(path).reply(200, mockResponse)
          await recipes.actions.getAllRecipes({
            commit: mockCommit,
            state: {
              list: recipesStateMock.list,
              minNumberOfPeople: 2,
              showOnlyVegetarian: recipesStateMock.showOnlyVegetarian,
              searchString: recipesStateMock.searchString
            }
          })
          expect(commit.count).toEqual(1)
        })

        it('it commits RECEIVE_RECIPES with search string filter applied', async () => {
          API.onGet(path).reply(200, mockResponse)
          await recipes.actions.getAllRecipes({
            commit: mockCommit,
            state: {
              list: recipesStateMock.list,
              minNumberOfPeople: recipesStateMock.minNumberOfPeople,
              showOnlyVegetarian: recipesStateMock.showOnlyVegetarian,
              searchString: 'testabc'
            }
          })
          expect(commit.count).toEqual(1)
        })
      })
      describe('onError', () => {
        let mockCommit = () => {
          commit.count += 1
        }

        it('does not commit anything', (done) => {
          API.onGet(path).reply(404, 'Not found')
          recipes.actions.getAllRecipes({commit: mockCommit, state: recipes.state})
            .catch(() => {
              expect(commit.count).toEqual(0)
              done()
            })
        })
      })
    })

    describe('filterVegetarian', () => {
      let mockCommit = (type) => {
        if (type === 'TOGGLE_VEGETARIAN') {
          commit.count += 1
        }
      }
      const mockDispatch = jest.fn()

      it('commits TOGGLE_VEGETARIAN and dispatches getAllRecipes', async () => {
        await recipes.actions.filterVegetarian({commit: mockCommit, dispatch: mockDispatch}, { vegetarian: true })
        expect(commit.count).toEqual(1)
        expect(mockDispatch).toHaveBeenCalledWith('getAllRecipes')
      })
    })

    describe('filterMinNumberOfPeople', () => {
      let mockCommit = (type) => {
        if (type === 'SET_MIN_NUMBER_OF_PEOPLE') {
          commit.count += 1
        }
      }
      const mockDispatch = jest.fn()

      it('commits SET_MIN_NUMBER_OF_PEOPLE and dispatches getAllRecipes', async () => {
        await recipes.actions.filterMinNumberOfPeople({commit: mockCommit, dispatch: mockDispatch}, { min: true })
        expect(commit.count).toEqual(1)
        expect(mockDispatch).toHaveBeenCalledWith('getAllRecipes')
      })
    })

    describe('setSearchString', () => {
      let mockCommit = (type) => {
        if (type === 'SET_SEARCH_STRING') {
          commit.count += 1
        }
      }
      const mockDispatch = jest.fn()

      it('commits SET_SEARCH_STRING and dispatches getAllRecipes', async () => {
        await recipes.actions.setSearchString({commit: mockCommit, dispatch: mockDispatch}, { searchString: 'test' })
        expect(commit.count).toEqual(1)
        expect(mockDispatch).toHaveBeenCalledWith('getAllRecipes')
      })
    })
  })

  describe('Mutations', () => {
    it('RECEIVE_RECIPES mutation sets the recipe list correctly', () => {
      recipes.mutations[types.RECEIVE_RECIPES](recipes.state, {list: recipesStateMock.list})
      expect(recipes.state.list).toEqual(recipesStateMock.list)
    })

    it('SET_MIN_NUMBER_OF_PEOPLE mutation sets the minimum number of people correctly', () => {
      recipes.mutations[types.SET_MIN_NUMBER_OF_PEOPLE](recipes.state, {min: 3})
      expect(recipes.state.minNumberOfPeople).toEqual(3)
    })

    it('SET_SEARCH_STRING mutation sets the search string correctly', () => {
      recipes.mutations[types.SET_SEARCH_STRING](recipes.state, {searchString: 'test'})
      expect(recipes.state.searchString).toEqual('test')
    })

    it('TOGGLE_VEGETARIAN mutation sets the show only vegetarian option correctly', () => {
      recipes.mutations[types.TOGGLE_VEGETARIAN](recipes.state, {vegetarian: true})
      expect(recipes.state.showOnlyVegetarian).toEqual(true)
    })
  })
})
