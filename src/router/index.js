import Vue from 'vue'
import VueRouter from 'vue-router'
import LandingPage from '../pages/Landing'
import RecipePage from '../pages/Recipe'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LandingPage',
      component: LandingPage
    },
    {
      path: '/recipe/:id',
      name: 'Recipe',
      component: RecipePage
    }
  ]
})
