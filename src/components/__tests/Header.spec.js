import { shallowMount, createLocalVue } from '@vue/test-utils'
import Header from '../Header'
import Vuetify from 'vuetify'

describe('Header', () => {
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    wrapper = shallowMount(Header, { localVue })
  })

  describe('When header is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('navigates to LandingPage on goToHomePage', () => {
      wrapper.vm.$router = { push: jest.fn() }
      wrapper.vm.goToHomePage()
      expect(wrapper.vm.$router.push).toHaveBeenCalledWith({ name: 'LandingPage' })
    })
  })
})
