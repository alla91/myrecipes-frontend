import { shallowMount, createLocalVue } from '@vue/test-utils'
import RecipeCard from '../RecipeCard'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('RecipeCard', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({
      getters: {
        'recipes/getSearchString': () => 'test'
      }
    })
    wrapper = shallowMount(RecipeCard, {
      store,
      localVue,
      propsData: {
        recipe: {
          id: 1,
          picture: 'pictureSrc'
        }
      }
    })
  })

  describe('When RecipeCard is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Methods', () => {
    it('navigates to RecipePage on goToRecipe', () => {
      wrapper.vm.$router = { push: jest.fn() }
      wrapper.vm.goToRecipe(1)
      expect(wrapper.vm.$router.push).toHaveBeenCalledWith({ name: 'Recipe', params: { id: 1 } })
    })
  })
})
