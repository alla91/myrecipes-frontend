import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from '../App'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

describe('App', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    localVue.use(VueRouter)
    store = new Vuex.Store({})
    wrapper = shallowMount(App, { store, localVue })
  })

  describe('When App is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
