import api from '../../api'
import * as types from '../mutation-types'

const namespaced = true

const state = {
  list: [],
  minNumberOfPeople: 0,
  showOnlyVegetarian: false,
  searchString: ''
}

const getters = {
  getRecipesList: state => state.list,
  getSearchString: state => state.searchString
}

const actions = {
  async getAllRecipes ({commit, state}) {
    await api.getAllRecipes(
      (response) => {
        let filteredRecipes = []
        if (!state.showOnlyVegetarian && state.minNumberOfPeople === 0 && state.searchString === '') {
          filteredRecipes = response.recipes
        } else {
          filteredRecipes = response.recipes.filter(recipe => {
            let valid = true
            if (state.showOnlyVegetarian && !JSON.parse(recipe.vegetarian)) {
              valid = false
            }
            if (state.minNumberOfPeople > 0 && parseInt(recipe.numberOfPeople) < state.minNumberOfPeople) {
              valid = false
            }
            if (state.searchString !== '' && !(recipe.name.toLowerCase().includes(state.searchString.toLowerCase()) || recipe.ingredients.toLowerCase().includes(state.searchString.toLowerCase()))) {
              valid = false
            }
            return valid
          })
        }
        commit(types.RECEIVE_RECIPES, {
          list: filteredRecipes
        })
        return Promise.resolve()
      },
      (error) => {
        return Promise.reject(error)
      }
    )
  },
  async filterVegetarian ({commit, dispatch}, { vegetarian }) {
    commit(types.TOGGLE_VEGETARIAN, {vegetarian: vegetarian})
    await dispatch('getAllRecipes')
  },
  async filterMinNumberOfPeople ({commit, dispatch}, { min }) {
    commit(types.SET_MIN_NUMBER_OF_PEOPLE, { min: min })
    await dispatch('getAllRecipes')
  },
  async setSearchString ({commit, dispatch}, { searchString }) {
    commit(types.SET_SEARCH_STRING, { searchString: searchString })
    await dispatch('getAllRecipes')
  }
}

const mutations = {
  [types.RECEIVE_RECIPES] (state, { list }) {
    state.list = list
  },
  [types.SET_MIN_NUMBER_OF_PEOPLE] (state, { min }) {
    state.minNumberOfPeople = min
  },
  [types.SET_SEARCH_STRING] (state, { searchString }) {
    state.searchString = searchString
  },
  [types.TOGGLE_VEGETARIAN] (state, { vegetarian }) {
    state.showOnlyVegetarian = vegetarian
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
