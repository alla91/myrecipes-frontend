import { shallowMount, createLocalVue } from '@vue/test-utils'
import Landing from '../Landing'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('Landing', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({
      getters: {
        'recipes/getRecipesList': () => []
      },
      actions: {
        'recipes/getAllRecipes': () => []
      }
    })
    wrapper = shallowMount(Landing, { store, localVue })
  })

  describe('When Landing page is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Computed', () => {
    describe('recipesList', () => {
      it('gets the correct value', () => {
        expect(wrapper.vm.recipesList).toEqual([])
      })
    })
  })
})
