import api from '../../api'
import * as types from '../mutation-types'

const namespaced = true

const state = {
  id: '',
  name: '',
  creationDate: '',
  picture: '',
  isVegetarian: false,
  numberOfPeople: 0,
  ingredients: '',
  cookingInstructions: ''
}

const getters = {
  getName: state => state.name,
  getCreationDate: state => state.creationDate,
  getPicture: state => state.picture,
  getIsVegetarian: state => state.isVegetarian,
  getNumberOfPeople: state => state.numberOfPeople,
  getIngredients: state => state.ingredients,
  getCookingInstructions: state => state.cookingInstructions
}

const actions = {
  async getRecipeById ({commit}, { id }) {
    await api.getRecipeById(
      id,
      (response) => {
        commit(types.RECEIVE_RECIPE, {
          recipe: response
        })
        return Promise.resolve()
      },
      (error) => {
        return Promise.reject(error)
      }
    )
  }
}

const mutations = {
  [types.RECEIVE_RECIPE] (state, { recipe }) {
    state.id = recipe._id
    state.name = recipe.name
    state.creationDate = recipe.creationDate
    state.picture = recipe.picture
    state.isVegetarian = JSON.parse(recipe.vegetarian)
    state.numberOfPeople = recipe.numberOfPeople
    state.ingredients = recipe.ingredients
    state.cookingInstructions = recipe.cookingInstructions
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
