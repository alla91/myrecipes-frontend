import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import recipe from '../../modules/recipe'
import recipeStateMock from '../mocks/recipeState'
import * as types from '../../mutation-types'
const hostname = 'http://localhost:3000'

describe('Recipes', () => {
  let API = new MockAdapter(axios)

  describe('State', () => {
    it('should contain the same keys as the mock', () => {
      let keysInModule = Object.keys(recipe.state)
      let keysInMock = Object.keys(recipeStateMock)
      expect(keysInModule).toEqual(keysInMock)
    })
  })

  describe('Getters', () => {
    let state
    beforeEach(() => {
      state = {...recipeStateMock}
    })

    it('returns the recipe name on getName', () => {
      const getterResult = recipe.getters.getName(state)
      expect(getterResult).toEqual(recipeStateMock.name)
    })

    it('returns the recipe creation date on getCreationDate', () => {
      const getterResult = recipe.getters.getCreationDate(state)
      expect(getterResult).toEqual(recipeStateMock.creationDate)
    })

    it('returns the recipe picture src on getPicture', () => {
      const getterResult = recipe.getters.getPicture(state)
      expect(getterResult).toEqual(recipeStateMock.picture)
    })

    it('returns whether or not the recipe is vegetarian on getIsVegetarian', () => {
      const getterResult = recipe.getters.getIsVegetarian(state)
      expect(getterResult).toEqual(recipeStateMock.isVegetarian)
    })

    it('returns the number of people for which the recipe is suitable on getNumberOfPeople', () => {
      const getterResult = recipe.getters.getNumberOfPeople(state)
      expect(getterResult).toEqual(recipeStateMock.numberOfPeople)
    })

    it('returns the recipe ingredients on getIngredients', () => {
      const getterResult = recipe.getters.getIngredients(state)
      expect(getterResult).toEqual(recipeStateMock.ingredients)
    })

    it('returns the recipe cooking instructions on getCookingInstructions', () => {
      const getterResult = recipe.getters.getCookingInstructions(state)
      expect(getterResult).toEqual(recipeStateMock.cookingInstructions)
    })
  })

  describe('Actions', () => {
    let commit = {
      count: 0,
      payload: ''
    }

    beforeEach(() => {
      commit.count = 0
      commit.payload = ''
    })

    afterEach(() => {
      API.reset()
    })

    describe('getRecipeById', () => {
      let path = `${hostname}/recipe/1`
      describe('onSuccess', () => {
        let mockCommit = (type) => {
          if (type === 'RECEIVE_RECIPE') {
            commit.count += 1
          }
        }

        it('commits RECEIVE_RECIPE', async () => {
          API.onGet(path).reply(200, recipeStateMock)
          await recipe.actions.getRecipeById({commit: mockCommit}, {id: '1'})
          expect(commit.count).toEqual(1)
        })
      })
      describe('onError', () => {
        let mockCommit = () => {
          commit.count += 1
        }

        it('does not commit anything', (done) => {
          API.onGet(path).reply(404, 'Not found')
          recipe.actions.getRecipeById({commit: mockCommit}, {id: '1'})
            .catch(() => {
              expect(commit.count).toEqual(0)
              done()
            })
        })
      })
    })
  })

  describe('Mutations', () => {
    let newRecipe = {...recipeStateMock, _id: recipeStateMock.id, vegetarian: recipeStateMock.isVegetarian}
    it('RECEIVE_RECIPE mutation sets the recipe details correctly', () => {
      recipe.mutations[types.RECEIVE_RECIPE](recipe.state, {recipe: newRecipe})
      expect(recipe.state.id).toEqual(recipeStateMock.id)
      expect(recipe.state.name).toEqual(recipeStateMock.name)
      expect(recipe.state.creationDate).toEqual(recipeStateMock.creationDate)
      expect(recipe.state.picture).toEqual(recipeStateMock.picture)
      expect(recipe.state.isVegetarian).toEqual(recipeStateMock.isVegetarian)
      expect(recipe.state.numberOfPeople).toEqual(recipeStateMock.numberOfPeople)
      expect(recipe.state.ingredients).toEqual(recipeStateMock.ingredients)
      expect(recipe.state.cookingInstructions).toEqual(recipeStateMock.cookingInstructions)
    })
  })
})
