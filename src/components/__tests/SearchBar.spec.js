import { shallowMount, createLocalVue } from '@vue/test-utils'
import SearchBar from '../SearchBar'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

describe('SearchBar', () => {
  let store
  let localVue
  let wrapper

  beforeEach(() => {
    localVue = createLocalVue()
    localVue.use(Vuetify, {})
    localVue.use(Vuex)
    store = new Vuex.Store({
      getters: {
        'recipes/getSearchString': () => 'test'
      }
    })
    wrapper = shallowMount(SearchBar, { store, localVue })
  })

  describe('When SearchBar is mounted', () => {
    it('has the expected html structure', () => {
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('Computed', () => {
    describe('searchString', () => {
      it('gets the correct value from store', () => {
        expect(wrapper.vm.searchString).toBe('test')
      })

      it('sets the correct value and dispatches setSearchString action', () => {
        wrapper.vm.$store.dispatch = jest.fn()
        wrapper.vm.searchString = 'test'
        expect(wrapper.vm.$store.dispatch).toHaveBeenCalledWith('recipes/setSearchString', { searchString: 'test' })
      })
    })
  })
})
